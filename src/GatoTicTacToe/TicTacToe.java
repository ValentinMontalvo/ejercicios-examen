package GatoTicTacToe;


import javax.swing.JOptionPane; 


/**
 *
 * @author valen
 */


public class TicTacToe extends javax.swing.JFrame {

    int v1, v2, v3, v4, v5, v6, v7, v8, v9;
    boolean done = false;
    boolean fin = false;

    public TicTacToe() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        casilla1 = new javax.swing.JButton();
        casilla2 = new javax.swing.JButton();
        casilla3 = new javax.swing.JButton();
        casilla4 = new javax.swing.JButton();
        casilla5 = new javax.swing.JButton();
        casilla6 = new javax.swing.JButton();
        casilla7 = new javax.swing.JButton();
        casilla8 = new javax.swing.JButton();
        casilla9 = new javax.swing.JButton();
        salida = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        casilla1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                casilla1ActionPerformed(evt);
            }
        });

        casilla2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                casilla2ActionPerformed(evt);
            }
        });

        casilla3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                casilla3ActionPerformed(evt);
            }
        });

        casilla4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                casilla4ActionPerformed(evt);
            }
        });

        casilla5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                casilla5ActionPerformed(evt);
            }
        });

        casilla6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                casilla6ActionPerformed(evt);
            }
        });

        casilla7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                casilla7ActionPerformed(evt);
            }
        });

        casilla8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                casilla8ActionPerformed(evt);
            }
        });

        casilla9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                casilla9ActionPerformed(evt);
            }
        });

        salida.setText("Salir");
        salida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salidaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(casilla1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(casilla2, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(casilla3, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(casilla4, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(casilla5, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(casilla6, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(casilla7, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(salida, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(casilla8, javax.swing.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(casilla9, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(casilla3, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(casilla2, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(casilla1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(casilla4, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(casilla5, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(casilla6, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(casilla7, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(casilla8, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(casilla9, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addComponent(salida)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void casilla1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_casilla1ActionPerformed

        gatoMaquina(2, 1);
        maquinaVirtual();

    }//GEN-LAST:event_casilla1ActionPerformed

    private void casilla2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_casilla2ActionPerformed

        gatoMaquina(2, 2);
        maquinaVirtual();


    }//GEN-LAST:event_casilla2ActionPerformed

    private void casilla3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_casilla3ActionPerformed
        
        gatoMaquina(2, 3);
        maquinaVirtual();
        
    }//GEN-LAST:event_casilla3ActionPerformed

    private void casilla4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_casilla4ActionPerformed
        
        gatoMaquina(2, 4);
        maquinaVirtual();
        
    }//GEN-LAST:event_casilla4ActionPerformed

    private void casilla5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_casilla5ActionPerformed
       
        gatoMaquina(2, 5);
        maquinaVirtual();
        
    }//GEN-LAST:event_casilla5ActionPerformed

    
    private void casilla6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_casilla6ActionPerformed
       
        gatoMaquina(2, 6);
        maquinaVirtual();
        
    }//GEN-LAST:event_casilla6ActionPerformed

    private void casilla7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_casilla7ActionPerformed
        
        gatoMaquina(2, 7);
        maquinaVirtual();
        
        
    }//GEN-LAST:event_casilla7ActionPerformed

    private void casilla8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_casilla8ActionPerformed
        
        gatoMaquina(2, 8);
        maquinaVirtual();
        
    }//GEN-LAST:event_casilla8ActionPerformed

    private void casilla9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_casilla9ActionPerformed
        
        gatoMaquina(2, 9);
        maquinaVirtual();
        
    }//GEN-LAST:event_casilla9ActionPerformed

    private void salidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salidaActionPerformed

        System.exit(0);
    }//GEN-LAST:event_salidaActionPerformed

    public void reset() {
        v1 = 0;
        v2 = 0;
        v3 = 0;
        v4 = 0;
        v5 = 0;
        v6 = 0;
        v7 = 0;
        v8 = 0;
        v9 = 0;
        casilla1.setText("");
        casilla1.setEnabled(true);
        casilla2.setText("");
        casilla2.setEnabled(true);
        casilla3.setText("");
        casilla3.setEnabled(true);
        casilla4.setText("");
        casilla4.setEnabled(true);
        casilla5.setText("");
        casilla5.setEnabled(true);
        casilla6.setText("");
        casilla6.setEnabled(true);
        casilla7.setText("");
        casilla7.setEnabled(true);
        casilla8.setText("");
        casilla8.setEnabled(true);
        casilla9.setText("");
        casilla9.setEnabled(true);
        done = false;
        fin = false;
    }

    public void gatoMaquina(int jugador, int posi) {

        switch (jugador) {
            case 1:
                switch (posi) {

                    case 1:
                        v1 = 1;
                        casilla1.setText("X");
                        casilla1.setEnabled(false);
                        break;
                    case 2:
                        v2 = 1;
                        casilla2.setText("X");
                        casilla2.setEnabled(false);
                        break;
                    case 3:
                        v3 = 1;
                        casilla3.setText("X");
                        casilla3.setEnabled(false);
                        break;
                    case 4:
                        v4 = 1;
                        casilla4.setText("X");
                        casilla4.setEnabled(false);
                        break;
                    case 5:
                        v5 = 1;
                        casilla5.setText("X");
                        casilla5.setEnabled(false);
                        break;
                    case 6:
                        v6 = 1;
                        casilla6.setText("X");
                        casilla6.setEnabled(false);
                        break;
                    case 7:
                        v7 = 1;
                        casilla7.setText("X");
                        casilla7.setEnabled(false);
                        break;
                    case 8:
                        v8 = 1;
                        casilla8.setText("X");
                        casilla8.setEnabled(false);
                        break;
                    case 9:
                        v9 = 1;
                        casilla9.setText("X");
                        casilla9.setEnabled(false);
                        break;
                }
                break;

            case 2:
                switch (posi) {
                    case 1:
                        v1 = 2;
                        casilla1.setText("O");
                        casilla1.setEnabled(false);
                        break;
                    case 2:
                        v2 = 2;
                        casilla2.setText("O");
                        casilla2.setEnabled(false);
                        break;
                    case 3:
                        v3 = 2;
                        casilla3.setText("O");
                        casilla3.setEnabled(false);
                        break;
                    case 4:
                        v4 = 2;
                        casilla4.setText("O");
                        casilla4.setEnabled(false);
                        break;
                    case 5:
                        v5 = 2;
                        casilla5.setText("O");
                        casilla5.setEnabled(false);
                        break;
                    case 6:
                        v6 = 2;
                        casilla6.setText("O");
                        casilla6.setEnabled(false);
                        break;
                    case 7:
                        v7 = 2;
                        casilla7.setText("O");
                        casilla7.setEnabled(false);
                        break;
                    case 8:
                        v8 = 2;
                        casilla8.setText("O");
                        casilla8.setEnabled(false);
                        break;
                    case 9:
                        v9 = 2;
                        casilla9.setText("O");
                        casilla9.setEnabled(false);
                        break;

                }
                break;

        }
    }

    public boolean verificarGanador(int jugador) {

        boolean gano = false;

        switch (jugador) {

            case 1:
                if (v1 == 1 && v2 == 1 && v3 == 1) {
                    gano = true;
                }
                if (v4 == 1 && v5 == 1 && v6 == 1) {
                    gano = true;
                }
                if (v7 == 1 && v8 == 1 && v9 == 1) {
                    gano = true;
                }
                if (v1 == 1 && v4 == 1 && v7 == 1) {
                    gano = true;
                }
                if (v2 == 1 && v5 == 1 && v8 == 1) {
                    gano = true;
                }
                if (v3 == 1 && v6 == 1 && v9 == 1) {
                    gano = true;
                }
                if (v1 == 1 && v5 == 1 && v9 == 1) {
                    gano = true;
                }
                if (v3 == 1 && v5 == 1 && v7 == 1) {
                    gano = true;
                }
                break;

            case 2:
                if (v1 == 2 && v2 == 2 && v3 == 2) {
                    gano = true;
                }
                if (v4 == 2 && v5 == 2 && v6 == 2) {
                    gano = true;
                }
                if (v7 == 2 && v8 == 2 && v9 == 2) {
                    gano = true;
                }
                if (v1 == 2 && v4 == 2 && v7 == 2) {
                    gano = true;
                }
                if (v2 == 2 && v5 == 2 && v8 == 2) {
                    gano = true;
                }
                if (v3 == 2 && v6 == 2 && v9 == 2) {
                    gano = true;
                }
                if (v1 == 2 && v5 == 2 && v9 == 2) {
                    gano = true;
                }
                if (v3 == 2 && v5 == 2 && v7 == 2) {
                    gano = true;
                }
                break;

        }
        return gano;
    }

    public void combinaciones() {

        //Primera fila horizontal
        if (v1 == 2 && v2 == 2 && v3 == 0 && !done) {
            gatoMaquina(1, 3);
            done = true;
        }
        if (v1 == 2 && v2 == 0 && v3 == 2 && !done) {
            gatoMaquina(1, 2);
            done = true;

        }
        if (v1 == 0 && v2 == 2 && v3 == 2 && !done) {
            gatoMaquina(1, 1);
            done = true;
        }

        //Segunda fila horizontal
        if (v4 == 2 && v5 == 2 && v6 == 0 && !done) {
            gatoMaquina(1, 6);
            done = true;
        }
        if (v4 == 2 && v5 == 0 && v6 == 2 && !done) {
            gatoMaquina(1, 5);
            done = true;
        }
        if (v4 == 0 && v5 == 2 && v6 == 2 && !done) {
            gatoMaquina(1, 4);
            done = true;
        }
        //Tercera fila horizontal
        if (v7 == 2 && v8 == 2 && v9 == 0 && !done) {
            gatoMaquina(1, 9);
            done = true;
        }
        if (v7 == 2 && v8 == 0 && v9 == 2 && !done) {
            gatoMaquina(1, 8);
            done = true;
        }
        if (v7 == 0 && v8 == 2 && v9 == 2 && !done) {
            gatoMaquina(1, 7);
            done = true;
        }
        //Primera vertical
        if (v1 == 2 && v4 == 2 && v7 == 0 && !done) {
            gatoMaquina(1, 7);
            done = true;
        }
        if (v1 == 2 && v4 == 0 && v7 == 2 && !done) {
            gatoMaquina(1, 4);
            done = true;
        }
        if (v1 == 0 && v4 == 2 && v7 == 2 && !done) {
            gatoMaquina(1, 1);
            done = true;
        }
        //Segunda vertical
        if (v2 == 2 && v5 == 2 && v8 == 0 && !done) {
            gatoMaquina(1, 8);
            done = true;
        }
        if (v2 == 2 && v5 == 0 && v8 == 2 && !done) {
            gatoMaquina(1, 5);
            done = true;
        }
        if (v2 == 0 && v5 == 2 && v8 == 2 && !done) {
            gatoMaquina(1, 2);
            done = true;
        }
        //Tercera vertical
        if (v3 == 2 && v6 == 2 && v9 == 0 && !done) {
            gatoMaquina(1, 9);
            done = true;
        }
        if (v3 == 2 && v6 == 0 && v9 == 2 && !done) {
            gatoMaquina(1, 6);
            done = true;
        }
        if (v3 == 0 && v6 == 2 && v9 == 2 && !done) {
            gatoMaquina(1, 3);
            done = true;
        }
        //Primera diagonal
        if (v1 == 2 && v5 == 2 && v9 == 0 && !done) {
            gatoMaquina(1, 9);
            done = true;
        }
        if (v1 == 2 && v5 == 0 && v9 == 2 && !done) {
            gatoMaquina(1, 5);
            done = true;
        }
        if (v1 == 0 && v5 == 2 && v9 == 2 && !done) {
            gatoMaquina(1, 1);
            done = true;
        }
        //Segunda diagonal
        if (v3 == 2 && v5 == 2 && v7 == 0 && !done) {
            gatoMaquina(1, 7);
            done = true;
        }
        if (v3 == 2 && v5 == 0 && v7 == 2 && !done) {
            gatoMaquina(1, 5);
            done = true;
        }
        if (v3 == 0 && v5 == 2 && v7 == 2 && !done) {
            gatoMaquina(1, 3);
            done = true;

        }

    }

    public void ganar() {
        

        if (v1 == 1 && v2 == 1 && v3 == 0 && !done) {
            gatoMaquina(1, 3);

            done = true;
        }
        if (v1 == 1 && v2 == 0 && v3 == 1 && !done) {
            gatoMaquina(1, 2);
            done = true;

        }
        if (v1 == 0 && v2 == 1 && v3 == 1 && !done) {
            gatoMaquina(1, 1);
            done = true;
        }

        //segunda fila horizontal
        if (v4 == 1 && v5 == 1 && v6 == 0 && !done) {
            gatoMaquina(1, 6);
            done = true;
        }
        if (v4 == 1 && v5 == 0 && v6 == 1 && !done) {
            gatoMaquina(1, 5);
            done = true;
        }
        if (v4 == 0 && v5 == 1 && v6 == 1 && !done) {
            gatoMaquina(1, 4);
            done = true;
        }
        //tercera fila horizontal
        if (v7 == 1 && v8 == 1 && v9 == 0 && !done) {
            gatoMaquina(1, 9);
            done = true;
        }
        if (v7 == 1 && v8 == 0 && v9 == 1 && !done) {
            gatoMaquina(1, 8);
            done = true;
        }
        if (v7 == 0 && v8 == 1 && v9 == 1 && !done) {
            gatoMaquina(1, 7);
            done = true;
        }
        //primera vertical
        if (v1 == 1 && v4 == 1 && v7 == 0 && !done) {
            gatoMaquina(1, 7);
            done = true;
        }
        if (v1 == 1 && v4 == 0 && v7 == 1 && !done) {
            gatoMaquina(1, 4);
            done = true;
        }
        if (v1 == 0 && v4 == 1 && v7 == 1 && !done) {
            gatoMaquina(1, 1);
            done = true;
        }
        //segunda vertical
        if (v2 == 1 && v5 == 1 && v8 == 0 && !done) {
            gatoMaquina(1, 8);
            done = true;
        }
        if (v2 == 1 && v5 == 0 && v8 == 1 && !done) {
            gatoMaquina(1, 5);
            done = true;
        }
        if (v2 == 0 && v5 == 1 && v8 == 1 && !done) {
            gatoMaquina(1, 2);
            done = true;
        }
        //tercera vertical
        if (v3 == 1 && v6 == 1 && v9 == 0 && !done) {
            gatoMaquina(1, 9);
            done = true;
        }
        if (v3 == 1 && v6 == 0 && v9 == 1 && !done) {
            gatoMaquina(1, 6);
            done = true;
        }
        if (v3 == 0 && v6 == 1 && v9 == 1 && !done) {
            gatoMaquina(1, 3);

            done = true;
        }
        //primera diagonal
        if (v1 == 1 && v5 == 1 && v9 == 0 && !done) {
            gatoMaquina(1, 9);
            done = true;
        }
        if (v1 == 1 && v5 == 0 && v9 == 1 && !done) {
            gatoMaquina(1, 5);
            done = true;
        }
        if (v1 == 0 && v5 == 1 && v9 == 1 && !done) {
            gatoMaquina(1, 1);
            done = true;
        }
        //segunda diagonal
        if (v3 == 1 && v5 == 1 && v7 == 0 && !done) {
            gatoMaquina(1, 7);
            done = true;
        }
        if (v3 == 1 && v5 == 0 && v7 == 1 && !done) {
            gatoMaquina(1, 5);
            done = true;
        }
        if (v3 == 0 && v5 == 1 && v7 == 1 && !done) {
            gatoMaquina(1, 3);

            done = true;
        }

    }

    public void centro() {

        if (v5 == 0 && !done) {
            gatoMaquina(1, 5);
            done = true;
        }

    }

    public void recorrer() {
        if (v1 == 0 && !done) {
            gatoMaquina(1, 1);
            done = true;
        }
        if (v2 == 0 && !done) {
            gatoMaquina(1, 2);
            done = true;
            System.out.println("2");
        }
        if (v3 == 0 && !done) {
            gatoMaquina(1, 3);
            done = true;
            System.out.println("3");
        }
        if (v4 == 0 && !done) {
            gatoMaquina(1, 4);
            done = true;
        }
        if (v5 == 0 && !done) {
            gatoMaquina(1, 5);
            done = true;
        }
        if (v6 == 0 && !done) {
            gatoMaquina(1, 6);
            done = true;
        }
        if (v7 == 0 && !done) {
            gatoMaquina(1, 7);
            done = true;
        }
        if (v8 == 0 && !done) {
            gatoMaquina(1, 8);
            done = true;
        }
        if (v9 == 0 && !done) {
            gatoMaquina(1, 9);
            done = true;
        }

    }

    public void allBloqueo() {
        if (v4 == 2 && v5 == 1 && v9 == 2 && v7 == 0 && !done) {
            gatoMaquina(1, 7);
            done = true;
        }
        if (v1 == 2 && v5 == 1 && v9 == 2 && v8 == 0 && !done) {
            gatoMaquina(1, 8);
            done = true;
        }
        if (v3 == 2 && v5 == 1 && v7 == 2 && v4 == 0 && !done) {
            gatoMaquina(1, 4);
            done = true;
        }
        if (v3 == 2 && v5 == 1 && v8 == 2 && v6 == 0 && !done) {
            gatoMaquina(1, 6);
            done = true;
        }
        if (v1 == 2 && v5 == 1 && v8 == 2 && v7 == 0 && !done) {
            gatoMaquina(1, 7);
            done = true;
        }

    }

    public void maquinaVirtual() {

        //Se marca que no hay ningun tiro realizado por parte de la maquina
        //Done cambia a true cuando la maquina tira
        
        done = false;
        fin = false;
        
        //Se almacena en gano el valor que recibe winner
        boolean gano = verificarGanador(2);

        //Comprueba si el jugador gano
        if (gano) {
            JOptionPane.showMessageDialog(null, "Que mala suerte", "Maquina", JOptionPane.PLAIN_MESSAGE);
            reset();
            fin = true;
        }

        //Esta funcion comprueba si puede ganar
        if (!done && !fin) {
            ganar();
        }

        //Intenta bloquear los tiros del ususario en caso de que se pueda
        if (!done && !fin) {
            combinaciones();
        }

        //Tira en el centro en caso de que este vacio
        if (!done && !fin) {
            
            centro();
        }

        if (!done && !fin) {
            allBloqueo();
        }

        //Si no puedo hacer nada de lo anterior
        if (!done && !fin && v3 == 0) {
            gatoMaquina(1, 3);
            done = true;
        }
        //Funcion  de comprobacion de casillas vacias

        if (!done && !fin) {
            recorrer();
        }

        //Comprueba si yo(maquina) gane
        if (done && !fin) {
            gano = verificarGanador(1);
            if (gano) {
                JOptionPane.showMessageDialog(null, "Soy el mejor", "Maquina", JOptionPane.PLAIN_MESSAGE);
                reset();
                fin = true;
            }

        }
        

        if (v1 != 0 && v2 != 0 && v3 != 0 && v4 != 0 && v5 != 0 && v6 != 0 && v7 != 0 && v8 != 0 && v9 != 0) {
            JOptionPane.showMessageDialog(null, "Nunca podras ganarme", "Maquina", JOptionPane.PLAIN_MESSAGE);
            reset();
        } 
        

    }
 
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TicTacToe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TicTacToe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TicTacToe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TicTacToe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        //System.out.println("");
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TicTacToe().setVisible(true);
            }
        });
    }
    
     
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton casilla1;
    private javax.swing.JButton casilla2;
    private javax.swing.JButton casilla3;
    private javax.swing.JButton casilla4;
    private javax.swing.JButton casilla5;
    private javax.swing.JButton casilla6;
    private javax.swing.JButton casilla7;
    private javax.swing.JButton casilla8;
    private javax.swing.JButton casilla9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton salida;
    // End of variables declaration//GEN-END:variables
}

