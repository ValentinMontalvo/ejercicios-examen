/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RelojTiempoReal;

import static java.lang.Thread.sleep;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author valen
 */
public class HoraTiempoReal extends Thread {

    javax.swing.JLabel horaActual;
    String fechaHora, am_pm;
    int contador;

    public HoraTiempoReal() {

        this.contador = 1;

    }
    
    
    public void run(){
        while(true){
            obtenerHoraTiempoReal();
            
            horaActual.setText(fechaHora+" "+am_pm);
            try {
                sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(HoraTiempoReal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
     public void recibeEtiqueta(javax.swing.JLabel horaImprimir){
       
       this.horaActual = horaImprimir;
    }
    
    public void obtenerHoraTiempoReal(){
        
        Calendar calendario = new GregorianCalendar();
        am_pm = calendario.get(Calendar.AM_PM)==Calendar.AM?"AM":"PM";
         
        Date hora = new Date();
        SimpleDateFormat formatoHora= new SimpleDateFormat("hh:mm:ss");
        fechaHora = formatoHora.format(hora);  
    }

}
